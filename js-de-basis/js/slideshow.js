var slideIndex = 1;

var initPage = function() {
    setTimeout(showSlides(slideIndex), 150);
}

var startScrolling = setTimeout(function() {
    setInterval(upSlide, 5000);
}, 155);

var upSlide = function() {
    changeSlides(1);
}
 
function changeSlides(n) {
  showSlides(slideIndex += n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1} 
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none"; 
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block"; 
  dots[slideIndex-1].className += " active";
}

window.onload = initPage;