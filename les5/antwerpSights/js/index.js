const myMap = L.map('map');

const myBasemap = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});

myBasemap.addTo(myMap);

myMap.setView([51.218769, 4.404798], 13);

const request = new XMLHttpRequest();
request.open('GET', 'https://api.myjson.com/bins/12p644', true);

request.onload = function() {
    const data = JSON.parse(this.response);

    const categoryCount = data.sights.reduce((sums, sight) => {
        sums[sight.category] = (sums[sight.category] || 0) + 1;
        return sums;
    }, {});

    const sidebar = document.getElementById('sights');
    const h3 = document.createElement("h3");
    h3.innerHTML = "Aantal Bezienswaardigheden";
    sidebar.appendChild(h3);

    for (let category in categoryCount) {
        const p = document.createElement("p");
        p.innerHTML = `<b>${category}</b> : ${categoryCount[category]}`;
        sidebar.appendChild(p);
    }

    const sights = data.sights.map(sight => {
        L.marker([sight.lat, sight.long]).bindPopup(`
        <h2>${sight.name}</h2>
        <p><b>Category:</b> ${sight.category}</p>
        `).openPopup().addTo(myMap);
    });
}  

request.send();