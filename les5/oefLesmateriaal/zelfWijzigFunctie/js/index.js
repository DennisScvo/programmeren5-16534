var user = 'Jef';
var authorization = function () {
    if (user == 'Jef') {
        return 'admin';
    }
    else {
        return 'bezoeker';
    }
}
console.log(user, ' is ', authorization());

var user = 'Jef';
var authorizationSelfModified = function () {
    if (user == 'Jef') {
        authorizationSelfModified = function () { return 'admin'; };
    }
    else {
        authorizationSelfModified = function () { return 'bezoeker'; };
    }
};

console.log(user, ' is ', authorizationSelfModified());
console.log(user, ' is ', authorizationSelfModified());