function objectToQuerystring(object) {
  return Object.keys(object).reduce(function(accumulator, key, currentIndex) {
    key = encodeURIComponent(key);
    var delimiter, val;
    delimiter = currentIndex === 0 ? "?" : "&";
    val = encodeURIComponent(object[key]);
    return [accumulator, delimiter, key, "=", val].join("");
  }, "");
}

var test = {
  Voornaam: "Jan",
  Familienaam: "Janssens",
  adres: "Boomstraat 38",
  stad: "Antwerpen"
};

console.log(objectToQuerystring(test));