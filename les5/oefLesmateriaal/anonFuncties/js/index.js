isPrime = function (value) {
    for (var i = 2; i < value; i++) {
        if (value % i === 0) {
            return false;
        }
    }
    return value > 1;
}

console.log('priemgetal?', isPrime(2071));

console.log('priemgetal anonieme functie?', (function (value) {
    for (var i = 2; i < value; i++) {
        if (value % i === 0) {
            return false;
        }
    }
    return value > 1;
})(7));