var array = [1, 2, 3, 4];

const sum = (acc, value) => acc + value;
const product = (acc, value) => acc * value;
const sumVerbose = (acc, value) => {
  const result = acc + value;
  document.getElementById('detail').innerHTML += acc + ' plus ' + value + ' is ' + result + '<br/>';
  return result;
}

function doSum() {
  clear();
  document.getElementById("resultSum").innerText = array.reduce(sum, 0);
}

function doSumVerbose() {
  clear();
  document.getElementById("resultSum").innerText = array.reduce(sumVerbose, 0);
}

function doProduct() {
  clear();
  document.getElementById("resultProduct").innerText = array.reduce(product, 1);
}

function clear() {
  document.getElementById('detail').innerHTML = '';
}