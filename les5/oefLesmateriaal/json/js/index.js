var personen = '{"persoon": [{"voornaam": "Peter","Familienaam": "Petersen"},{"voornaam": "Jef","Familienaam": "Jefsen"},{"voornaam": "Tom","Familienaam": "Tomsen"}]}';

var persoonObject = JSON.parse(personen);
console.log(persoonObject['persoon'][1].voornaam);

persoonObject.persoon[1].voornaam = 'Geoff';

console.log(JSON.stringify(persoonObject));

var pe = document.createElement('P');
var t = document.createTextNode('test');

pe.appendChild(t);
document.getElementById('p').appendChild(pe);
div = document.createElement('DIV');
div.innerHTML = `<em> ${persoonObject['persoon'][1].voornaam}</em>`;
document.body.appendChild(div);