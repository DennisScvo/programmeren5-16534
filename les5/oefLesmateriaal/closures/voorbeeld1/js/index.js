(function() {
  console.log('hello van IIFE');
})();

var something = 'hello';
setTimeout(function() {
  console.log(something);
}, 1000);

setTimeout((function(something) {
  return function() {
    console.log(something);
  }
})(something), 1000);

something = 'tot ziens';