function init(x) {
   var i = 0;
   return function () {
      return x[i++];
   };
}
var volgende = init([1, 5, 10, 15]);
console.log(volgende());
console.log(volgende());
console.log(volgende());
console.log(volgende());
console.log(volgende());