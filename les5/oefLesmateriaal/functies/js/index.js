var voornaam = function () { return 'Jean-Paul' };
var naam = [function () { return 'Hugo' }, 'Claus'];
var persoon = {
    leeftijd: 80, voornaam: 'Karel',
    familienaam: function () { return 'Van Miert' }
};
var naam = 'Karel ' + (function () { return 'De Grote' })();
function sayHello(voornaam) {
    return 'Hello ' + voornaam();
}

function say(voornaam) { return (function () { return 'Hello ' + voornaam; }) }