var wifiConnection;

var app = {
    initialize: function () {
        vos.setModel();
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },
    onDeviceReady: function () {
        this.receivedEvent('deviceready');
        document.addEventListener("backbutton", controller.page.previous, false);
        document.addEventListener('online', this.onOnline, false);
        document.addEventListener('offline', this.onOffline, false);
        document.body.addEventListener('click', dispatcher, false);
        this.setWifiState();
        readPersistenceFile();
        trySilentSignin();
    },
    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');
        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');
        console.log('Received Event: ' + id);
    },
    onOnline: function () {
        app.setWifiState();
        if(wifiConnection === true) {
            app.flushDelayedLogItems();
        }
    },
    onOffline: function () {
        wifiConnection = false;
    },
    setWifiState: function() {
        var networkState = navigator.connection.type;
        if(networkState === Connection.WIFI) {
            wifiConnection = true;
        } else {
            wifiConnection = false;
        }
    },
    flushDelayedLogItems: function() {
        var logItemsBacklog = vos.model.delayedLogItems;
            if(logItemsBacklog.length > 0) {
                for (const item of logItemsBacklog) {
                    logger.logAction(item);
                }
                vos.model.delayedLogItems = [];
                removePersistenceFile();
            }
    }
};

app.initialize();

var phoneCall = function (number) {
    var success = function(result) {
        console.log('The call was successful: ', result);
        logger.updateLogInfo("timestamp", Date.now());
        logger.logAction(JSON.stringify(vos.model.logItem));
    }
    var error = function(result) {
        console.log('The call was unsuccessful: ', result);
    }

    window.plugins.CallNumber.callNumber(success, error, number, false);
};

var smsSend = function (number, message, item) {
    var options = {
        replaceLineBreaks: true,
        android: {
            intent: 'INTENT'
        }
    };

    var success = function () {
        alert('SMS is verstuurd!');
        logger.updateLogInfo("timestamp", Date.now());
        logger.logAction(JSON.stringify(vos.model.logItem));
    };

    var error = function (e) {
        alert('SMS is niet verstuurd: ' + e);
    };
    if (typeof sms === 'undefined' || typeof sms.send === 'undefined') {
        alert('SMS send is undefined. Would have sent error');
    } else {
        sms.send(number, message, options, success, error);       
    }
};

var logger = {
    logAction: function (logItem) {
        if(wifiConnection === true) {
            $http('https://vosapidenniss.azurewebsites.net/api/log')
            .post(logItem);
        } else {
            vos.model.delayedLogItems.push(logItem);
            updatePersistenceFile(vos.model.delayedLogItems);
            console.log('in memory: ', logItem);
        }
    },
    updateLogInfo: function(key, value) {
        vos.model.logItem[key] = value;
    }
}