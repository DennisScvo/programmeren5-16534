var makeTextElement = function(text, tag) {
    if (!tag) {
        tag = 'P';
    }
    var elem = document.createElement(tag);
    var textNode = document.createTextNode(text);
    elem.appendChild(textNode);
    return elem;
}

/**
 * Een html element creëren van het type opgegeven in de tag parameter.
 * Plaats de tekst met html opmaak opgegeven in de text parameter in het gemaakte element.
 *
 * @param {string} text Text to be placed in the html element.
 * @param {string} tag type van het te maken html element.
 */
var makeHtmlTextElement = function(text, tag) {
    if (!tag) {
        tag = 'P';
    }
    var elem = document.createElement(tag);
    elem.innerHTML = text;
    return elem;
}

/**
 * Een button html element maken met een specifieke tekst erin en een bepaald icoon.
 *
 * @param {string} text Text to be placed in the button element.
 * @param {string} icon klassenaam van het te tonen icoon.
 */
var makeTileButton = function(text, icon) {
    var buttonElem = document.createElement('BUTTON');
    buttonElem.setAttribute('type', 'submit');
    buttonElem.setAttribute('class', 'tile');
    var iconElem = document.createElement('SPAN');
    iconElem.setAttribute('class', icon);
    buttonElem.appendChild(iconElem);
    var screenReaderTextElem = document.createElement('SPAN');
    screenReaderTextElem.setAttribute('class', 'screen-reader-text');
    var textElem = document.createTextNode(text);
    screenReaderTextElem.appendChild(textElem);
    buttonElem.appendChild(screenReaderTextElem);
    return buttonElem;
}

function makeProfileImage(url) {
    var elem = document.createElement('img');
    elem.setAttribute('src', url);
    elem.setAttribute('id', 'profile-img');
    return elem;
}

var getPhoneNumber = function(number) {
    var phoneNumber = number;
    switch (number) {
        case 'DV' :
            phoneNumber = vos.model.myLocation.directie.phone;
            break;
        case 'secretariaat' :
            phoneNumber = vos.model.myLocation.secretariaat.phone;
            break;
        case 'preventieadviseur' :
            phoneNumber = vos.model.myLocation.preventieadviseur.phone;
            break;
    }
    return phoneNumber;
}

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371;
    var dLat = deg2rad(lat2-lat1);
    var dLon = deg2rad(lon2-lon1);
    var a =
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI/180)
}

function TEL(item) {
    var phoneNumber = getPhoneNumber(item.action[0].phoneNumber);
    if (vos.model.identity.loggedIn) {
        var buttonElement = makeTileButton('Tel', 'icon-phone');
        buttonElement.setAttribute('class', 'action-button');
        buttonElement.addEventListener('click', function () {
            logger.updateLogInfo("stapTitle", item.title);
            logger.updateLogInfo("targetTelefoonnummer", phoneNumber);
            phoneCall(phoneNumber);
        });
        return buttonElement;
    } else {
        return makeTextElement(item.code + ' ' + phoneNumber, 'P');
    }
}

function SMS(item, message) {
    var phoneNumber = getPhoneNumber(item.action[0].phoneNumber);
    if (vos.model.identity.loggedIn) {
        var buttonElement = makeTileButton('Tel', 'icon-send');
        buttonElement.setAttribute('class', 'action-button');
        buttonElement.addEventListener('click', function () {
            logger.updateLogInfo("stapTitle", item.title);
            logger.updateLogInfo("targetTelefoonnummer", phoneNumber);
            vos.smsPrepare(phoneNumber, message, item);
        });
        return buttonElement;
    } else {
        return makeTextElement(item.code + ' ' + phoneNumber, 'P');
    }
}

function updatePersistenceFile(dataEntry) {
    window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dirEntry) {
        dirEntry.getFile('delayedLogs.json', { create : true}, function (fileEntry) {
            fileEntry.createWriter(function (fileWriter) {
                fileWriter.onwriteend = function() {
                    console.log('Added to persistence: ', dataEntry);
                };
                fileWriter.onerror = function (e) {
                    console.log("Failed to read file: ", e.toString());
                };
                fileWriter.write(dataEntry);
            });
        });
    });
}

function readPersistenceFile() {
    window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dirEntry) {
        dirEntry.getFile('delayedLogs.json', {create : false}, function (fileEntry) {
            fileEntry.file(function (file) {
                var reader = new FileReader();
                reader.onloadend = function() {
                    vos.model.delayedLogItems = JSON.parse(this.result);
                    var networkState = navigator.connection.type;
                    if(networkState === Connection.WIFI) {
                        app.flushDelayedLogItems();
                    }
                }
                reader.readAsText(file);
            });
        }, function(err) {
            console.log('Did not delete persistence file: ', err.code);
        });
    });
}

function removePersistenceFile() {
    window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dirEntry) {
        dirEntry.getFile('delayedLogs.json', {create: false}, function (fileEntry) {
            fileEntry.remove(function (file) {
                console.log('File removed...');
            }, function (err) {
                console.log('An error occured while remove the file: ', err.code);
            }, function() {
                console.log('File does not seem to exist...')
            });
        });
    });
}