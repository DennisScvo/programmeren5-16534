function $http(url) {

    var core = {

        ajax: function (method, url, args) {

            var promise = new Promise(function (resolve, reject) {

                var client = new XMLHttpRequest();
                var uri = url;
                client.open(method, uri);
                if (args && (method === 'POST' || method === 'PUT')) {
                    client.setRequestHeader("Content-Type", "application/json");
                    client.send(args);
                } else {
                    client.send(args);
                }
                client.onload = function () {
                    if (this.status == 200 || this.status == 201) {
                        resolve(this.response);
                    }
                    else {
                        reject(this.statusText + this.responseText);
                    }
                };
                client.onerror = function () {
                    reject(this.statusText);
                };
            });
            return promise;
        }
    };

    return {
        'get': function (args) {
            return core.ajax('GET', url, args);
        },
        'post': function (args) {
            return core.ajax('POST', url, args);
        },
        'put': function (args) {
            return core.ajax('PUT', url, args);
        },
        'delete': function (args) {
            return core.ajax('DELETE', url, args);
        }
    };
};
var payload = {
    'topic': 'js',
    'q': 'Promise'
};

var callback = {
    success: function (data) {
        var pre = document.createElement('PRE');
        var t = document.createTextNode(data);
        pre.appendChild(t);
        document.body.appendChild(pre);
    },
    error: function (data) {
        var pre = document.createElement('PRE');
        var t = document.createTextNode('<b>' + 2 + '</b> error ' + data);
        pre.appendChild(t);
        document.body.appendChild(pre);
    }
};