var vos = {
    'model': {
        loaded: false,
        identity: {},
        procedureList: {},
        organisationList: {},
        position: {},
        myLocation: {},
        logItem: {},
        delayedLogItems: []
    },
    'setModel': function () {

        window.localStorage.clear();
        localStorage.removeItem('model');
        var model = JSON.parse(localStorage.getItem("model"));
        if (model === null) {
            $http('data/identity.json')
                .get()
                .then(function (data) {
                    vos.model.identity = JSON.parse(data);
                    var payload = {};
                    var fileName = 'data/procedure' + vos.model.identity.role.toUpperCase() + '.json';
                    return $http(fileName).get(payload);
                })
                .then(function (data) {
                    vos.model.procedureList = JSON.parse(data);
                    var payload = { 'id': 1 };
                    return $http('data/position.json').get(payload);
                })
                .then(function (data) {
                    vos.model.position = JSON.parse(data);
                    var payload = {};
                    return $http('data/organisationList.json').get(payload);
                })
                .then(function (data) {
                    vos.model.organisationList = JSON.parse(data);
                    vos.model.loaded = true;
                    localStorage.setItem('model', JSON.stringify(vos.model));
                    controller['home']['index']();
                })
                .catch(function (data) {
                    vos.model.loaded = false;
                    localStorage.setItem('model', JSON.stringify(vos.model));
                });
        } else {
            vos.model = model;
            controller['home']['index']();
        }
    },
    navigateTo: function (view, title) {
        location.href = '#' + view;
        var h1 = document.querySelector('#' + view + ' h1');
        if (title && h1) {
            h1.innerHTML = title;
        }
    },
    login: function () {
        signin();
    },
    logout: function () {
        signout();
    },
    getPosition: function () {
        var options = {
            maximumAge: 3600000,
            timeout: 20000,
            enableHighAccuracy: true
        }
        var onSuccess = function (pos) {
            vos.model.position.latitude = pos.coords.latitude.toFixed(4);
            vos.model.position.longitude = pos.coords.longitude.toFixed(4);
            vos.setMyLocation();
        };
        var onError = function (error) {
            console.log(error);
            vos.model.position.latitude = 51.1771;
            vos.model.position.longitude = 4.3533;
            vos.setMyLocation();
        };
        navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
    },
    setMyLocation: function () {
        vos.model.organisationList.forEach(function (item) {
            item.distanceFromMyLocation = getDistanceFromLatLonInKm(
                vos.model.position.latitude, vos.model.position.longitude,
                item.latitude, item.longitude);
        });
        vos.model.organisationList.sort(function (a, b) {
            return a.distanceFromMyLocation - b.distanceFromMyLocation;
        });
        vos.model.myLocation = vos.model.organisationList[0];
        render.identity('#home .identity');
        view['home']['index']();
    },
    smsPrepare: function (number, messageText, item) {
        var message = messageText + '\n' +
            vos.model.identity.firstName + ' ' + vos.model.identity.lastName + '\n' +
            vos.model.myLocation.name + '\n' +
            vos.model.myLocation.street + '\n' +
            vos.model.myLocation.postalcode + ' ' + vos.model.myLocation.city + '\n' +
            vos.model.identity.mobile;
        smsSend(number, message, item);
    }
}

var render = {
    'identity': function (querySelector) {
        var elem = document.querySelector(querySelector);
        elem.innerHTML = '';
        if (vos.model.identity.loggedIn) {
            elem.appendChild(makeProfileImage(vos.model.identity.imageUrl));
            logger.updateLogInfo("user", vos.model.identity.userName);
            logger.updateLogInfo("email", vos.model.identity.email);
            logger.updateLogInfo("rol", vos.model.identity.role);
            logger.updateLogInfo("sourceTelefoonnummer", vos.model.identity.mobile);
        }
        elem.appendChild(makeTextElement(vos.model.identity.firstName + ' ' + vos.model.identity.lastName, 'h2'))
        elem.appendChild(makeTextElement(vos.model.identity.function, 'h3'));
        elem.appendChild(makeTextElement(vos.model.identity.mobile, 'h4'));
        elem.appendChild(makeTextElement(' --- ', 'h4'));
        elem.appendChild(makeTextElement(vos.model.myLocation.name));
        elem.appendChild(makeTextElement(vos.model.myLocation.street));
        elem.appendChild(makeTextElement(vos.model.myLocation.postalcode + ' ' + vos.model.myLocation.city));
        elem.appendChild(makeTextElement(vos.model.myLocation.phone));
        if (vos.model.identity.loggedIn) {
            elem.appendChild(makeTextElement('Aangemeld als ' + vos.model.identity.role));
        }
        else {
            elem.appendChild(makeTextElement('Niet aangemeld'));
        }
        return elem;
    },
    'login': function (querySelector) {
        var elem = document.querySelector(querySelector);
        elem.innerHTML = '';
        if (vos.model.identity.loggedIn) {
            var buttonElement = makeTileButton('Afmelden', 'icon-exit');
            buttonElement.setAttribute('name', 'uc');
            buttonElement.setAttribute('value', 'home/logout');
            elem.appendChild(buttonElement);
        }
        else {
            var buttonElement = makeTileButton('Aanmelden', 'icon-enter');
            buttonElement.setAttribute('name', 'uc');
            buttonElement.setAttribute('value', 'home/login');
            elem.appendChild(buttonElement);
        }
    },
    'procedure': { 
        'make': function (procedureCode) {
            var procedure = vos.model.procedureList.procedure.find(function (item) {
                return item.code === procedureCode;
            });
            logger.updateLogInfo("procedureTitel", procedure.title);
            logger.updateLogInfo("procedureCode", procedureCode);
            render.identity('.view-identity-tile');
            let procedureBody = document.querySelector('#procedure-body');
            let header = makeHtmlTextElement(procedure.heading, 'h2');
            procedureBody.appendChild(header);
            
            procedure.step.forEach(function (item) {
                let stepBlock = document.createElement('DIV');
                stepBlock.setAttribute('class', 'step-block');
                let subtitle = makeHtmlTextElement(item.title, 'h3');
                subtitle.setAttribute('class', 'procedure-subtitle');
                stepBlock.appendChild(subtitle);
                if("list" in item) {
                    item.list.forEach(function (listItem) {
                        let stepLine = makeHtmlTextElement(listItem.title, 'h5');
                        stepLine.setAttribute('class', 'step-list-item');
                        stepBlock.appendChild(stepLine);
                    });
                }
                if("action" in item && vos.model.identity.loggedIn) {
                    item.action.forEach(function (actionItem) {
                        if(actionItem.code === 'SMS') {
                            let actionButton = SMS(item, "Er is een bommelding.");
                            stepBlock.appendChild(actionButton);
                        }
                        if(actionItem.code === 'TEL') {
                            let actionButton = TEL(item);
                            stepBlock.appendChild(actionButton);
                        }
                    });
                } else if("action" in item && !vos.model.identity.loggedIn){
                    let userInfo = makeHtmlTextElement('Voor deze functionaliteit dient u in te loggen.', 'h5');
                    stepBlock.appendChild(userInfo);
                }
                procedureBody.appendChild(stepBlock);
            });
        }
    }
};

var dispatcher = function (e) {
    var target = e.target;
    var steps = 0;
    while (target.getAttribute('name') !== 'uc' && steps < 5 && target.tagName !== 'BODY') {
        target = target.parentNode;
        steps++;
    }
    if (target.getAttribute('name') === 'uc') {
        var uc = target.getAttribute('value');
        var path = uc.split('/');
        var entity = path[0] === undefined ? 'none' : path[0];
        var action = path[1] === undefined ? 'none' : path[1];
        var view = entity + '-' + action;
        if (controller[entity][action]) {
            controller[entity][action]();
        } else {
            alert('ongeldige url ' + uc);
        }
    }
};

var view = {
    'home': {
        'index': function () {
            window.location.assign("index.html#home-index");
        }
    },
    'psycho-social-risk': {
        'index': function () {
            window.location.href = "#psycho-social-index";
        }
    },
    'fire': {
        'index': function () {
            window.location.href = "#fire-index";
        }
    }
    ,
    'terror': {
        'index': function () {
            window.location.href = "#terror-index";
        }
    },
    'accident': {
        'index': function () {
            window.location.href = "#accident-index";
        }
    },
    'procedure': function (title) {
        vos.navigateTo('view-procedure', title);
    }
};

var controller = {
    'home': {
        'index': function () {
            vos.setMyLocation();
            render.identity('#home .identity');
            render.login('#home-cp .login');
            view['home']['index']();
        },
        'gas-leak': function () {
            render.procedure.make('GL');
            view['procedure']('gaslek');
        },
        'amok': function () {
            render.procedure.make('AMOK');
            view['procedure']('AMOK - Geweld');
        },
        'login': function () {
            vos.login();
            vos.getPosition();
            render.login('#home-cp .login');
            view['home']['index']();
        },
        'logout': function () {
            vos.logout();
            render.identity('#home .identity');
            render.login('#home-cp .login');
            view['home']['index']();
        },
        'settings': vos.settings
    },
    'call': {
        'hot-line': function () {
            var phoneNumber = '0472473071';
            phoneCall(phoneNumber);
            logger.updateLogInfo("stapTitle", "HOTLINE-CALL");
            logger.updateLogInfo("targetTelefoonnummer", phoneNumber);
            logger.updateLogInfo("procedureTitel", "Home hotline");
            logger.updateLogInfo("procedureCode", "HL");
        }
    },
    'psycho-social-risk': {
        'index': function () {
            render.identity('#psycho-social-risk .identity');
            render.login('#psycho-cp .login');
            view['psycho-social-risk']['index']();
        }
    },
    'terror': {
        'index': function () {
            render.identity('#terror .identity');
            render.login('#terror-cp .login');
            view['terror']['index']();
        },
        'bomb-alarm': function () {
            render.procedure.make('BA');
            view['procedure']('Bomalarm');
        },
        'suspicious-object': function () {
            render.procedure.make('VV');
            view['procedure']('Verdacht voorwerp');
        },
        'terrorist-attack': function () {
            render.procedure.make('TA');
            view['procedure']('Terroristische aanslag');
        },
        'amok': function () {
            render.procedure.make('AMOK');
            view['procedure']('AMOK & blind geweld');
        }
    }
    ,
    'accident': {
        'index': function () {
            render.identity('#accident .identity');
            render.login('#accident-cp .login');
            view['accident']['index']();
        },
        'extra-muros': function () {
            render.procedure.make('EM');
            view['procedure']('extra-muros');
        },
        'serious-work-accident': function () {
            render.procedure.make('SWA');
            view['procedure']('ernstig arbeidsongeval');
        },
        'work-accident': function () {
            render.procedure.make('WA');
            view['procedure']('arbeidsongeval');
        },
        'to-from-school': function () {
            render.procedure.make('TFS');
            view['procedure']('van en naar school');
        }
    },
    'fire': {
        'index': function () {
            render.identity('#fire .identity');
            render.login('#fire-cp .login');
            view['fire']['index']('Brand');
        },
        'detection': function () {
            render.procedure.make('BM');
            view['procedure']('brandmelding');
        },
        'evacuation': function () {
            render.procedure.make('BREV');
            view['procedure']('brandevacuatie');
        }
    },
    'page': {
        'previous': function () {
            window.history.back();
            let procedureBody = document.querySelector('#procedure-body');
            procedureBody.innerHTML = '';
        }
    }
};