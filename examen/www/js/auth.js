var clientId = "134136991674-f5d3ffoti89oso808pj7ie624er986mo.apps.googleusercontent.com";

function trySilentSignin() {
    window.plugins.googleplus.trySilentLogin(
        {
            'webCliendId': clientId
        },
        function(response) {
            vos.model.identity.loggedIn = true;
            console.log(response);
            showUserProfile(response);
            vos.getPosition();
            render.identity('#home .identity');
            render.login('#home-cp .login');
            view['home']['index']();
            controller['home']['index']();
        },
        function(msg) {
            vos.model.identity.loggedIn = false;
        }
    )
}

function signin() {
    window.plugins.googleplus.login(
        {
            'webCliendId': clientId
        },
        function (response) {
            console.log(response);
            showUserProfile(response);
            vos.model.identity.loggedIn = true;
            controller['home']['index']();
        },
        function (error) {
            console.log('error: ' + error);
        }
    )
}

function signout() {
    window.plugins.googleplus.logout(
        function (msg) {
            console.log('Signed out from google.');
            vos.model.identity.loggedIn = false;
            vos.setModel();
            controller['home']['index']();
        }
    )
}

function showUserProfile(profile) {
    let userId = profile.userId;
    let imageUrl = profile.imageUrl;
    if (userId) {
        $http('data/personList.json')
            .get()
            .then(function (responseText) {
                var person = JSON.parse(responseText);
                var userIdentity = person.list.find(function (item) {
                    return item.code === userId;
                });
                if (userIdentity) {
                    vos.model.identity = userIdentity;
                    if(profile.imageUrl) {
                        vos.model.identity.imageUrl = imageUrl;
                    } else {
                        vos.model.identity.imageUrl = "img/vos.jpg";
                    }
                    var payload = {};
                    var fileName = 'data/procedure' + vos.model.identity.role.toUpperCase() + '.json';
                    $http(fileName).get(payload)
                        .then(function (data) {
                            vos.model.procedureList = JSON.parse(data);
                            localStorage.setItem('model', JSON.stringify(vos.model));
                            controller['home']['index']();
                        });
                } else {
                    vos.model.identity.firstName = "Onbekende";
                    vos.model.identity.lastName = "Gebruiker";
                    if(profile.imageUrl) {
                        vos.model.identity.imageUrl = imageUrl;
                    } else {
                        vos.model.identity.imageUrl = "img/vos.jpg";
                    }
                    alert('U bent niet gekend als gebruiker binnen onze databank. U kan verdergaan als Gast.')
                    localStorage.setItem('model', JSON.stringify(vos.model));
                    controller['home']['index']();
                }
            })
            .catch(function (message) {
                alert(message);
            })
    } else {
        alert('Geen gegevens teruggekregen van Google. U kan verdergaan als Gast.');
    }
}