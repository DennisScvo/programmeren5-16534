var app = {
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    onDeviceReady: function () {
        this.receivedEvent('deviceready');
    },

    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();

var phoneCall = function (number) {
    // if (window.cordova) {
    //     //ref = window.open('tel:' + number, '_system');
    //     cordova.InAppBrowser.open('tel:' + number.replace(/\s/g,''), '_system');
    //     //ref.addEventListener("endcallbutton", onEndCallKeyDown, false);
    // }
    if (window.cordova) {
        var bypassAppChooser = true;
        window.plugins.CallNumber.callNumber(onPhoneCallSuccess, onPhoneCallError, number, bypassAppChooser);
    }
};

function onPhoneCallSuccess(result) {
    // alert('succes: ' + result);
}

function onPhoneCallError(result) {
    // alert('error: ' + result);
}

var smsSend = function (number, message) {
    // CONFIGURATION
    var options = {
        replaceLineBreaks: false, // true to replace \n by a new line, false by default
        android: {
            intent: 'INTENT'  // send SMS with the native android SMS messaging
            // intent: '' // send SMS without open any other app
        }
    };

    var success = function () {
        alert('SMS is verstuurd!');
    };

    var error = function (e) {
        alert('SMS is niet verstuurd:' + e);
    };
    if (typeof sms === 'undefined' || typeof sms.send === 'undefined') {
        alert('SMS send is undefined. Would have sent error');
    } else {
        sms.send(number, message, options, success, error);
    }
};

vos.setModel();
document.body.addEventListener('click', dispatcher, false);