var clientId = "134136991674-1fiav17f0qcv1df27vd5fgess5knl283.apps.googleusercontent.com";
var apiKey = "AIzaSyCvfapCgf6mbIK7W0pS8ylzlpT5aZFLGro";
var scopes = "profile";

let signinButton;
let signoutButton;

function handleClientLoad() {
    gapi.load('client:auth2', initAuth);
}

function initAuth() {
    gapi.auth2.init({
        'apiKey': apiKey,
        client_id: clientId,
        scope: scopes
    }).then(function () {
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    });
}

function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
        makePeopleApiCall();
        vos.model.identity.loggedIn = true;
    } else {
        vos.model.identity.loggedIn = false;
        vos.setModel();
        controller['home']['index']();
    }
}

function signin() {
    gapi.auth2.getAuthInstance().signIn();
}

function signout() {
    gapi.auth2.getAuthInstance().signOut();
}

function makePeopleApiCall() {
    gapi.client.load('people', 'v1', function () {
        let request = gapi.client.people.people.get({
            resourceName: 'people/me',
            'requestMask.includeField': 'person.names'
        });
        request.execute(function (resp) {
            showUserProfile();
        });
    });
}

function showUserProfile() {
    let fullName;
    let userId;
    let imageUrl;
    const profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
    fullName = profile.getName();
    userId = profile.getId();
    imageUrl = profile.getImageUrl();
    if (userId) {
        $http('data/personList.json')
            .get()
            .then(function (responseText) {
                var person = JSON.parse(responseText);
                var userIdentity = person.list.find(function (item) {
                    return item.code === userId;
                });
                if (userIdentity) {
                    // identity = JSON.parse(localStorage.getItem('identity'));
                    vos.model.identity = userIdentity;
                    vos.model.identity.imageUrl = imageUrl;
                    var payload = {};
                    // procedures depend on Role (in uppercase)
                    var fileName = 'data/procedure' + vos.model.identity.role.toUpperCase() + '.json';
                    $http(fileName).get(payload)
                        .then(function (data) {
                            vos.model.procedureList = JSON.parse(data);
                            localStorage.setItem('model', JSON.stringify(vos.model));
                            controller['home']['index']();
                        });
                } else {
                    var splitName = fullName.split(" ");
                    vos.model.identity.firstName = splitName[0];
                    vos.model.identity.lastName = splitName[1];
                    vos.model.identity.imageUrl = imageUrl;
                    alert('U bent niet gekend als gebruiker binnen onze databank. U kan verdergaan als Gast.')
                    controller['home']['index']();
                }
            })
            .catch(function (message) {
                alert(message);
            })
    } else {
        alert('Geen gegevens teruggekregen van Google. U kan verdergaan als Gast.');
    }
}