--Dennis Slavid
--14/01/2019

USE Vos;
DROP PROCEDURE IF EXISTS insert_log;
DROP PROCEDURE IF EXISTS get_all_logs;
DROP PROCEDURE IF EXISTS get_log_by_id;
DROP PROCEDURE IF EXISTS get_log_by_user;
DROP PROCEDURE IF EXISTS update_user;
DROP PROCEDURE IF EXISTS delete_log_by_id;

DELIMITER //
CREATE PROCEDURE insert_log(IN identifier INT, IN user NVARCHAR(50))
BEGIN
    INSERT INTO Log(id, user_name)
    VALUES(identifier, user);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_all_logs()
BEGIN
    SELECT * FROM Log;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_log_by_id(IN identifier INT)
BEGIN
    SELECT * FROM Log
    WHERE id = identifier;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_log_by_user(IN user NVARCHAR(50))
BEGIN
    SELECT * FROM Log
    WHERE user_name = user;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE update_user(IN identifier INT, IN user NVARCHAR(50))
BEGIN
    UPDATE Log
    SET user_name = user
    WHERE id = identifier;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE delete_log_by_id(IN identifier INT)
BEGIN
    DELETE FROM Log
    WHERE id = identifier;
END //
DELIMITER ;