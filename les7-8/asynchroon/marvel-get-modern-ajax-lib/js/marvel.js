var loadMarvelCharacters = function (that) {
    var timeStamp = new Date().getTime();
    var url = "https://gateway.marvel.com:443/v1/public/characters?limit=100&apikey=eb7e313a0b822de773ff50ed365ad9a1&ts=" + timeStamp + "&hash=" + 
                generateHash(timeStamp + "669290664aff5c23bb3fdb9dd490ab9c939b4893eb7e313a0b822de773ff50ed365ad9a1");
    $http(url)
        .get(payload)
        .then(data => {
            charactersCall.success(data);
        }).catch(err => {
            charactersCall.error(err);
        })
    ;
}

var charactersCall = {
    success: function(response) {
        var characters = JSON.parse(response).data.results;
        var table = document.createElement('table');
        table.id = 'prettyTable';
        characters.map(function (character) {
            var row = document.createElement('tr');
            var name = document.createElement('td');
            var textContent = document.createTextNode(character.name);
            var imageCol = document.createElement('td');
            var image = document.createElement('img');
            image.src = character.thumbnail.path + '.' + character.thumbnail.extension;
            image.id = 'thumb';
            imageCol.appendChild(image);
            name.appendChild(textContent);
            row.appendChild(name);
            row.appendChild(imageCol);
            table.appendChild(row);
        });
    document.body.appendChild(table);
    },
    error: function(err) {
        var errorHeader = document.createElement('h1');
        var errorMessage = document.createElement('p');
        var headerTextContent = document.createTextNode('Whoops, something went terribly wrong :(');
        var messageTextContent = document.createTextNode(err);
        errorHeader.appendChild(headerTextContent);
        errorMessage.appendChild(messageTextContent);
        document.body.appendChild(errorHeader);
        document.body.appendChild(errorMessage);
    }
}

window.onload = loadMarvelCharacters;