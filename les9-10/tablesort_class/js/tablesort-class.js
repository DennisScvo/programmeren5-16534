class TableSort {
    constructor(id) {
        this.tableElement = document.getElementById(id);
        if (this.tableElement && this.tableElement.nodeName == "TABLE") {
            this.prepare();
        }
    }

    prepare() {
        let headings = this.tableElement.tHead.rows[0].cells;
        for (let i = 0; i < headings.length; i++) {
            headings[i].innerHTML = headings[i].innerHTML + '<span>&nbsp;&nbsp;&uarr;&darr;</span>';
        }
        this.tableElement.addEventListener("click", function (that) {
            return function (event) {
                that.eventHandler(event);
            }
        }(this), false);
    }

    eventHandler(event) {
        if (event.target.tagName === 'TH') {
            let arrow = event.target.getElementsByTagName("span")[0];
            if (event.target.className == "sorted-asc") {
                event.target.className = "sorted-desc";
                arrow.innerHTML = "&nbsp;&nbsp;&darr;"
                this.sortColumn(event.target, "sorted-desc");
            } else {
                this.resetTableHeaders();
                event.target.className = 'sorted-asc';
                arrow.innerHTML = "&nbsp;&nbsp;&uarr;"
                this.sortColumn(event.target);
            };
        } else if (event.target.tagName === 'SPAN') {
            if (event.target.parentNode.tagName === 'TH') {
                if (event.target.parentNode.className == "sorted-asc") {
                    event.target.parentNode.className = 'sorted-desc';
                    event.target.innerHTML = "&nbsp;&nbsp;&darr;"
                    this.sortColumn(event.target.parentNode, 'sorted-desc');
                } else {
                    this.resetTableHeaders();
                    event.target.parentNode.className = 'sorted-asc';
                    event.target.innerHTML = "&nbsp;&nbsp;&uarr;"
                    this.sortColumn(event.target.parentNode);
                };
            }
        }
    }

    sortColumn(headerCell, sortDirection = "asc") {
        let rows = this.tableElement.rows;
        let alpha = [],
            numeric = [];
        let alphaIndex = 0,
            numericIndex = 0;
        let cellIndex = headerCell.cellIndex;
        for (var i = 1; rows[i]; i++) {
            let cell = rows[i].cells[cellIndex];
            let content = cell.textContent ? cell.textContent : cell.innerText;
            let numericValue = content.replace(/(\$|\,|\s)/g, "");
            if (parseFloat(numericValue) == numericValue) {
                numeric[numericIndex++] = {
                    value: Number(numericValue),
                    row: rows[i]
                }
            } else {
                alpha[alphaIndex++] = {
                    value: content,
                    row: rows[i]
                }
            }
        }
        let orderdedColumns = [];
        numeric.sort(function (a, b) {
            if (sortDirection === "asc") {
                return a.value - b.value;
            } else {
                return b.value - a.value;
            }
        });
        alpha.sort(function (a, b) {
            let aName = a.value.toLowerCase();
            let bName = b.value.toLowerCase();
            if (sortDirection === "asc") {
                if (aName < bName) {
                    return -1
                } else if (aName > bName) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                if (aName > bName) {
                    return -1
                } else if (aName < bName) {
                    return 1;
                } else {
                    return 0;
                }
            }

        });
        orderdedColumns = numeric.concat(alpha);
        let tBody = this.tableElement.tBodies[0];
        for (let i = 0; orderdedColumns[i]; i++) {
            tBody.appendChild(orderdedColumns[i].row);
        }
    }

    resetTableHeaders() {
        let allColumnHeaders = this.tableElement.getElementsByTagName("th");
        for (let cell of allColumnHeaders) {
            if (cell.className == "sorted-asc") {
                let cellArrow = cell.getElementsByTagName("span")[0];
                cellArrow.innerHTML = "&nbsp;&nbsp;&uarr;&darr;"
                cell.classList.remove("sorted-asc");
            } else if (cell.className == "sorted-desc") {
                let cellArrow = cell.getElementsByTagName("span")[0];
                cellArrow.innerHTML = "&nbsp;&nbsp;&uarr;&darr;"
                cell.classList.remove("sorted-desc");
            }
        }
    }
}

window.onload = function () {
    var jommeke = new TableSort("jommeke");
    var fruit = new TableSort("fruit");
}