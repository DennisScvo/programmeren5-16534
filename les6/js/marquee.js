var scrollDelay = 2000;
var previousSpeed = 1;
var marqueeSpeed = 1;
var timer;

var scrollArea;
var marquee;
var scrollPosition = 0;

document.addEventListener("mouseover", handleMouseOverEvent, false);
document.addEventListener("mouseout", handleMouseOutEvent, false);
document.addEventListener("click", handleOnClickEvent, true);

function handleMouseOverEvent(event) {
    if(event.target.id == 'scroll-area' || event.target.id == 'marquee') {
        pauseMarquee();
    }
}

function handleMouseOutEvent(event) {
    if(event.target.id == 'scroll-area' || event.target.id == 'marquee') {
        restartMarquee();
    }
}

function handleOnClickEvent(event) {
    if(event.target.id == 'speedUpButton') {
        speedUpMarquee();
    } else if(event.target.id == 'slowDownButton') {
        slowDownMarquee();
    }
}

var scrolling = function () {
    if (scrollPosition + scrollArea.offsetHeight <= 0) {
        scrollPosition = marquee.offsetHeight;
    } else {
        scrollPosition = scrollPosition - marqueeSpeed;
    }
    scrollArea.style.top = scrollPosition + "px";
}

var startScrolling = function () {
    timer = setInterval(scrolling, 30);
}

var initMarquee = function() {
    scrollArea = document.getElementById("scroll-area");
    scrollArea.style.top = 0;
    marquee = document.getElementById("marquee");
    setTimeout(startScrolling, scrollDelay);
}

var pauseMarquee = function () {
    previousSpeed = marqueeSpeed;
    if (marqueeSpeed > 0) {
        marqueeSpeed = 0;
    }
}

var restartMarquee = function () {
    marqueeSpeed = previousSpeed;
}

var speedUpMarquee = function () {
    if (marqueeSpeed <= 5) {
        marqueeSpeed++;
    } 
}

var slowDownMarquee = function () {
    if (marqueeSpeed > 1) {
        marqueeSpeed--;
    }
}

window.onload = initMarquee;

