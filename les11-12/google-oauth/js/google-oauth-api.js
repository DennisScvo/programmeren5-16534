var clientId = "134136991674-1fiav17f0qcv1df27vd5fgess5knl283.apps.googleusercontent.com";
var apiKey = "AIzaSyCvfapCgf6mbIK7W0pS8ylzlpT5aZFLGro";
var scopes = "profile";

let signinButton;
let signoutButton;

function handleClientLoad() {
    gapi.load('client:auth2', initAuth);
}

function initAuth() {
    gapi.auth2.init({
        'apiKey': apiKey,
        client_id: clientId,
        scope: scopes
    }).then(function () {
        signinButton = document.getElementById('signin-button');
        signoutButton = document.getElementById('signout-button');
        signinButton.addEventListener('click', signin);
        signoutButton.addEventListener('click', signout);
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    });
}

function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
        makePeopleApiCall();
        signinButton.style.display = 'none';
        signoutButton.style.display = 'block';
    } else {
        signinButton.style.display = 'block';
        signoutButton.style.display = 'none';
    }
}

function signin(event) {
    gapi.auth2.getAuthInstance().signIn();
}

function signout(event) {
    removeUserProfile();
    gapi.auth2.getAuthInstance().signOut();
}

function makePeopleApiCall() {
    gapi.client.load('people', 'v1', function () {
        let request = gapi.client.people.people.get({
            resourceName: 'people/me',
            'requestMask.includeField': 'person.names'
        });
        request.execute(function (resp) {
            let p = document.createElement('p');
            p.id = 'greeting';
            if (resp.names) {
                var name = resp.names[0].givenName;
            }
            else {
                var name = 'Geen naam gevonden';
            }
            p.appendChild(document.createTextNode('Hello, ' + name + '!'));
            document.getElementById('content').appendChild(p);
            showUserProfile();
        });
    });
}

function showUserProfile() {
    const profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
    let h1 = document.createElement('h1');
    h1.id = 'profile-id';
    h1.appendChild(document.createTextNode(profile.getId()));
    document.getElementById('content').appendChild(h1);
    let h2 = document.createElement('h2');
    h2.id = 'profile-name';
    h2.appendChild(document.createTextNode(profile.getName()));
    document.getElementById('content').appendChild(h2);
    let h3 = document.createElement('h3');
    h3.id = 'first-name';
    h3.appendChild(document.createTextNode(profile.getGivenName()));
    document.getElementById('content').appendChild(h3);
    let h4 = document.createElement('h4');
    h4.id = 'last-name';
    h4.appendChild(document.createTextNode(profile.getFamilyName()));
    document.getElementById('content').appendChild(h4);
    let img = document.createElement('img');
    img.id = 'image';
    img.setAttribute("src", profile.getImageUrl());
    document.getElementById('content').appendChild(img);
    let h5 = document.createElement('h5');
    h5.id = 'email';
    h5.appendChild(document.createTextNode(profile.getEmail()));
    document.getElementById('content').appendChild(h5);
}

function removeUserProfile() {
    let greeting = document.getElementById('greeting');
    let profileId = document.getElementById('profile-id');
    let profileName = document.getElementById('profile-name');
    let firstName = document.getElementById('first-name');
    let lastName = document.getElementById('last-name');
    let image = document.getElementById('image');
    let email = document.getElementById('email');
    greeting.remove();
    profileId.remove();
    profileName.remove();
    firstName.remove();
    lastName.remove();
    image.remove();
    email.remove();
}